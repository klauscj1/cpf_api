//const Usuario = require("../models/Usuario");
const bcrypt = require("bcryptjs");
const { generarJWT } = require("../helpers/jwt");
const { response } = require("express");
/*
const {
  buscarUsuarioPorEmail,
  guardarUsuario,
} = require("../database/repositories/usuario_repo");*/
const crearUsuario = async (req, res = response) => {
  try {
    const { email, password } = req.body;
    /* let usuario = await buscarUsuarioPorEmail(email);
    if (usuario) {
      return res.status(400).json({
        ok: false,
        msg: "El correo ya esta utilizado con otro usuario",
      });
    }*/
    //Encriptar contraseña
    const salt = bcrypt.genSaltSync();
    const passwordEncrypt = bcrypt.hashSync(password, salt);

    const usuario = {
      email,
      password: passwordEncrypt,
    };

    /*const respuesta = await guardarUsuario(usuario);

    if (!respuesta) {
      return res.status(500).json({
        ok: false,
        msg: "El servidor esta con problemas",
      });
    }
    usuario = await buscarUsuarioPorEmail(email);*/
    //Generar nuestro jwt
    const token = await generarJWT(usuario);
    res.status(201).json({
      ok: true,
      token,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: "Por favor hable con el admin.",
    });
  }
};

const loginUsuario = async (req, res = response) => {
  const { email, password, password_encriptado } = req.body;

  try {
    /*
    let usuario = await buscarUsuarioPorEmail(email);
    if (!usuario) {
      return res.status(400).json({
        ok: false,
        msg: "El usuario no existe con ese email",
      });
    }*/

    const usuario = {
      email,
      password,
    };
    const validarPassword = bcrypt.compareSync(password, password_encriptado);

    if (!validarPassword) {
      return res.status(400).json({
        ok: false,
        msg: "La contraseña incorrecta",
      });
    }

    //Generar nuestro jwt

    const token = await generarJWT(usuario);

    res.status(200).json({
      ok: true,
      token,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: "Por favor hable con el admin.",
    });
  }
};

const revalidarToken = async (req, res = response) => {
  const uid = req.uid;
  const name = req.name;

  const token = await generarJWT(uid, name);
  res.json({
    ok: true,
    token,
    uid,
    name,
  });
};
module.exports = { crearUsuario, loginUsuario, revalidarToken };
