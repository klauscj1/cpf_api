const pool = require("../db");
const buscarUsuarioPorEmail = async (email) => {
  try {
    const usuario = await pool.query(
      "select * from usuario where usu_email=$1 ",
      [email]
    );
    return usuario.rows[0];
  } catch (error) {
    return null;
  }
};

const buscarUsuarioPorId = async (id) => {
  try {
    const usuario = await pool.query("select * from usuario where usu_id=$1 ", [
      id,
    ]);
    return usuario.rows[0];
  } catch (error) {
    return null;
  }
};

const guardarUsuario = async (usuario) => {
  try {
    const usuarioSave = await pool.query(
      "insert into usuario(usu_email,usu_password,usu_nombre,usu_tipo,usu_activo) values ($1,$2,$3,$4,$5)",
      [
        usuario.email,
        usuario.password,
        usuario.name,
        usuario.tipo,
        usuario.activo,
      ]
    );
    return usuarioSave.rowCount;
  } catch (error) {
    console.log(error);
    return null;
  }
};

module.exports = {
  buscarUsuarioPorEmail,
  guardarUsuario,
  buscarUsuarioPorId,
};
