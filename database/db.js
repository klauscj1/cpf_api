const Pool = require("pg").Pool;

const pool = new Pool({
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME,
});
module.exports = pool;

//SQL DE LA TABLA USUARIO

// CREATE TABLE public.usuario
// (
//     usu_id SERIAL,
//     usu_email text COLLATE pg_catalog."default",
//     usu_nombre text COLLATE pg_catalog."default",
//     usu_password text COLLATE pg_catalog."default",
//     usu_tipo integer,
//     usu_activo boolean DEFAULT true,
//     usu_fecha_ingreso timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
//     CONSTRAINT usuario_pkey PRIMARY KEY (usu_id)
// )

// TABLESPACE pg_default;
