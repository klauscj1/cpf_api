const jwt = require("jsonwebtoken");
const generarJWT = (user) => {
  return new Promise((resolve, reject) => {
    const payload = { ...user };
    jwt.sign(
      payload,
      process.env.SECRET_JWT_SEDD,
      {
        expiresIn: "8h",
      },
      (error, token) => {
        if (error) {
          console.log(error);
          reject("No se pudo generar el token");
        }
        resolve(token);
      }
    );
  });
};

module.exports = {
  generarJWT,
};
