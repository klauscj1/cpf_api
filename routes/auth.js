/*
  Rutas de usuarios / Auth
  host + /api/auth
*/
const { Router } = require("express");
const { check } = require("express-validator");
const router = Router();
const {
  crearUsuario,
  loginUsuario,
  revalidarToken,
} = require("../controllers/auth");
const { validarCampos } = require("../middlewares/validar-campos");
const { validarJWT } = require("../middlewares/validar-jwt");

router.post(
  "/",
  [
    check("email", "El email es obligatorio").isEmail(),
    check(
      "password",
      "La contrasena es obligatoria y debe de ser de 6 o mas caracteres"
    ).isLength({ min: 6 }),
    validarCampos,
  ],
  crearUsuario
);
router.post(
  "/login",
  [
    check("email", "El email es obligatorio").isEmail(),
    check(
      "password",
      "La contrasena es obligatoria y debe de ser de 6 o mas caracteres"
    ).isLength({ min: 6 }),
    validarCampos,
  ],
  loginUsuario
);
router.get("/renew", validarJWT, revalidarToken);

module.exports = router;
